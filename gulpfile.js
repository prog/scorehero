const gulp = require("gulp");
const cssnano = require("gulp-cssnano");
const less = require("gulp-less");
const livereload = require("gulp-livereload");
const rename = require("gulp-rename");
const sourcemaps = require("gulp-sourcemaps");
const watch = require("gulp-watch");

const resPath = "public/res/";
const lessBase = resPath + "less/";



function errorHandler(err) {
	console.error(err.message);
	this.emit("end");
}



function startLivereload() {
	livereload.listen();
}



function css_dev() {
	return gulp
			.src(lessBase + "index.less")
			.pipe(sourcemaps.init())
			.pipe(less({relativeUrls: true, rootpath: "res/"})).on("error", errorHandler)
			.pipe(cssnano({discardUnused: {fontFace: false}}))
			.pipe(rename("build.css"))
			.pipe(sourcemaps.write(".", {sourceRoot: "less/"}))
			.pipe(gulp.dest(resPath))
			.pipe(livereload());
}



function css_prod() {
	return css_dev();
}



function css_watch() {
	startLivereload();
	gulp.watch(lessBase + "**/*.less", css_dev);
}



gulp.task("css-dev", css_dev);
gulp.task("css-prod", css_prod);
gulp.task("css-watch", css_watch);
