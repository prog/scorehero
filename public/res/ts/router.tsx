import * as React from "react";
import { Router, Route, browserHistory } from "react-router";

import Dashboard from "./views/Dashboard";



export default (
		<Router history={browserHistory}>
			<Route path="/" component={Dashboard} />
		</Router>
);
