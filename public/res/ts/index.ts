import * as React from "react";
const reactDOM = require("react-dom");
const injectTapEventPlugin = require("react-tap-event-plugin");

import router from "./router.tsx";



injectTapEventPlugin();
reactDOM.render(router, document.getElementById("app-root"));
