module.exports = {
	entry: "./public/res/ts/index",
	output: {
		path: "./public/res/",
		publicPath: "/res/",
		filename: "build.js"
	},
	resolve: {
		extensions: ["", ".tsx", ".ts", ".js"]
	},
	module: {
		loaders: [
			{test: /\.tsx?$/, loader: "ts-loader?", exclude: /node_modules/}
		]
	},
	devtool: "source-map"
};
